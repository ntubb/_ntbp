<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _ntbp
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div class="row">
			<div class="small-12 columns">
				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title"><i class="fa fa-fw fa-lg fa-chain-broken"></i><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'freshdental' ); ?></h1>
					</header><!-- .page-header -->
					<div class="page-content">
						<p><em><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links in the main menu or a search?', 'freshdental' ); ?></em></p>
						<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="button"><?php _e('Go Back to the Homepage', 'freshdental') ?></a></p>
						<?php get_search_form(); ?>

					</div><!-- .page-content -->
				</section><!-- .error-404 -->
			</div><!-- .columns -->
		</div><!-- .row -->

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
