<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package _ntbp
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function _ntbp_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', '_ntbp_body_classes' );

if ( ! function_exists( '_ntbp_edit_post_link' ) ) :

/* 
 * Add button class to edit_post_link function.
 */
function _ntbp_edit_post_link($output) {
	$output = str_replace('class="post-edit-link"', 'class="post-edit-link button tiny secondary round"', $output);
	return $output;
}
add_filter('edit_post_link', '_ntbp_edit_post_link');
endif;

if ( ! function_exists( '_ntbp_edit_post_link' ) ) :
	
/* 
 * Add button class to edit_post_link function.
 */
function _ntbp_edit_post_link($output) {
	$output = str_replace('class="post-edit-link"', 'class="post-edit-link button tiny secondary round"', $output);
	return $output;
}
add_filter('edit_post_link', '_ntbp_edit_post_link');
endif;

if ( ! function_exists( '_ntbp_edit_comment_link' ) ) :

/* 
 * Add button class to edit_comment_link function.
 */
function _ntbp_edit_comment_link($output) {
	$output = str_replace('class="comment-edit-link"', 'class="comment-edit-link button tiny secondary round"', $output);
	return $output;
}
add_filter('edit_comment_link', '_ntbp_edit_comment_link');
endif;

if ( ! function_exists( '_ntbp_comment_reply_link' ) ) :

/* 
 * Add button class to comment_reply_link function.
 */
function _ntbp_comment_reply_link($output) {
	$output = str_replace('class=\'comment-reply-link\'', 'class="comment-reply-link button tiny round"', $output);
	$output = str_replace('class="comment-reply-login"', 'class="comment-reply-login button tiny round"', $output);
	return $output;
}
add_filter('comment_reply_link', '_ntbp_comment_reply_link');
endif;

if ( ! function_exists( '_ntbp_cancel_comment_reply_link' ) ) :

/* 
 * Add button class to cancel_comment_reply_link function.
 */
function _ntbp_cancel_comment_reply_link($output) {
	$output = str_replace('id="cancel-comment-reply-link"', 'id="cancel-comment-reply-link" class="cancel-comment-reply-link button tiny round alert"', $output);
	return $output;
}
add_filter('cancel_comment_reply_link', '_ntbp_cancel_comment_reply_link');
endif;

if ( ! function_exists( '_ntbp_posts_link_attributes' ) ) :

/* 
 * Adding button classes to next and prev posts links.
 */
function _ntbp_posts_link_attributes() {
	return 'class="button small round"';
}
add_filter('next_posts_link_attributes', '_ntbp_posts_link_attributes');
add_filter('previous_posts_link_attributes', '_ntbp_posts_link_attributes');
endif;

if ( ! function_exists( '_ntbp_post_link_attributes' ) ) :
/* 
 * Adding button classes to next and prev post links. 
 */
function _ntbp_post_link_attributes($output) {
	$injection = 'class="button small round"';
	return str_replace('<a href=', '<a '.$injection.' href=', $output);
}
add_filter('next_post_link', '_ntbp_post_link_attributes');
add_filter('previous_post_link', '_ntbp_post_link_attributes');
endif;