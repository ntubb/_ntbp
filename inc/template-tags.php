<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package _ntbp
 */

if ( ! function_exists( '_ntbp_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 */
function _ntbp_paging_nav() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation paging-navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', '_ntbp' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous left"><?php next_posts_link( __( '<span class="meta-nav">&laquo;</span> Older posts', '_ntbp' ) ); ?></div>
		<?php endif; ?>

		<?php if ( get_previous_posts_link() ) : ?>
		<div class="nav-next right"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&raquo;</span>', '_ntbp' ) ); ?></div>
	<?php endif; ?>

</div><!-- .nav-links -->
</nav><!-- .navigation -->
<?php
}
endif;

if ( ! function_exists( '_ntbp_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 */
function _ntbp_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation post-navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', '_ntbp' ); ?></h1>
		<div class="nav-links">
			<?php
			previous_post_link( '<div class="nav-previous left ">%link</div>', _x( '<span class="meta-nav">&laquo;</span>&nbsp;%title', 'Previous post link', '_ntbp' ) );
			next_post_link(     '<div class="nav-next right">%link</div>',     _x( '%title&nbsp;<span class="meta-nav">&raquo;</span>', 'Next post link',     '_ntbp' ) );
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( '_ntbp_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function _ntbp_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
		);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', '_ntbp' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', '_ntbp' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( '_ntbp_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function _ntbp_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', '_ntbp' ) );
		if ( $categories_list && _ntbp_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', '_ntbp' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', '_ntbp' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', '_ntbp' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', '_ntbp' ), esc_html__( '1 Comment', '_ntbp' ), esc_html__( '% Comments', '_ntbp' ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', '_ntbp' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
		'<span class="edit-link">',
		'</span>'
		);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function _ntbp_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( '_ntbp_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
			) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( '_ntbp_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so _ntbp_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so _ntbp_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in _ntbp_categorized_blog.
 */
function _ntbp_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( '_ntbp_categories' );
}
add_action( 'edit_category', '_ntbp_category_transient_flusher' );
add_action( 'save_post',     '_ntbp_category_transient_flusher' );

/**
 * Walker for blog comments list.
 *
 */
if ( ! class_exists( '_ntbp_Comments' ) ) :
	class _ntbp_Comments extends Walker_Comment{

	// Init classwide variables.
		var $tree_type = 'comment';
		var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );

	/** CONSTRUCTOR
	 * You'll have to use this if you plan to get to the top of the comments list, as
	 * start_lvl() only goes as high as 1 deep nested comments */
	function __construct() { ?>

		<h3><?php comments_number( __( 'No Responses to', '_ntbp' ), __( 'One Response to', '_ntbp' ), __( '% Responses to', '_ntbp' ) ); ?> &#8220;<?php the_title(); ?>&#8221;</h3>
		<ol class="comment-list">

			<?php }

	/** START_LVL
	 * Starts the list before the CHILD elements are added. */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1; ?>

		<ul class="children">
			<?php }

	/** END_LVL
	 * Ends the children list of after the elements are added. */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1; ?>

	</ul><!-- /.children -->

	<?php }

	/** START_EL */
	function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment;
		$parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' ); ?>

		<li <?php comment_class( $parent_class ); ?> id="comment-<?php comment_ID() ?>">
			<article id="comment-body-<?php comment_ID() ?>" class="comment-body">



				<header class="comment-author">

					<?php echo get_avatar( $comment, $args['avatar_size'] ); ?>

					<div class="author-meta vcard author">

						<?php printf( __( '<cite class="fn">%s</cite>', '_ntbp' ), get_comment_author_link() ) ?>
						<time datetime="<?php echo comment_date( 'c' ) ?>"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf( __( '%1$s', '_ntbp' ), get_comment_date(),  get_comment_time() ) ?></a></time>

					</div><!-- /.comment-author -->

				</header>

				<section id="comment-content-<?php comment_ID(); ?>" class="comment">
					<?php if ( ! $comment->comment_approved ) : ?>
					<div class="notice">
						<p class="bottom"><?php _e( 'Your comment is awaiting moderation.' ); ?></p>
					</div>
				<?php else : comment_text(); ?>
			<?php endif; ?>
		</section><!-- /.comment-content -->

		<div class="comment-meta comment-meta-data hide">
			<a href="<?php echo htmlspecialchars( get_comment_link( get_comment_ID() ) ) ?>"><?php comment_date(); ?> at <?php comment_time(); ?></a> <?php edit_comment_link( '(Edit)' ); ?>
		</div><!-- /.comment-meta -->

		<div class="reply">
			<?php $reply_args = array(
				'depth' => $depth,
				'max_depth' => $args['max_depth'],
				);

			comment_reply_link( array_merge( $args, $reply_args ) );  ?>
		</div><!-- /.reply -->
	</article><!-- /.comment-body -->

	<?php }

	function end_el(& $output, $comment, $depth = 0, $args = array() ) { ?>

	</li><!-- /#comment-' . get_comment_ID() . ' -->

	<?php }

	/** DESTRUCTOR */
	function __destruct() { ?>

	</ol><!-- /#comment-list -->

	<?php }
}
endif;

/**
* Returns the social button list for.
*/
function _ntbp_get_social_button_list() {
	$output = '';
	if(get_field('options_facebook_url','options')
		|| get_field('options_google_plus_url','options')
		|| get_field('options_twitter_url','options')
		|| get_field('options_linkedin_url','options')
		|| get_field('options_pinterest_url','options')
		|| get_field('options_instagram_url','options')){
		$output .= '<ul class="social-button-list">';
	if(get_field('options_facebook_url','options')) {$output .= '<li><a class="social-button" href="'.get_field('options_facebook_url','options').'" title="'.__('Follow Us On Facebook','_ntbp').'" target="_blank"><i class="fa fa-fw fa-facebook"></i></a></li>';}
	if(get_field('options_google_plus_url','options')) {$output .= '<li><a class="social-button" href="'.get_field('options_google_plus_url','options').'" title="'.__('Follow Us On Google Plus','_ntbp').'" target="_blank"><i class="fa fa-fw fa-google-plus"></i></a></li>';}
	if(get_field('options_twitter_url','options')) {$output .= '<li><a class="social-button" href="'.get_field('options_twitter_url','options').'" title="'.__('Follow Us On Twitter','_ntbp').'" target="_blank"><i class="fa fa-fw fa-twitter"></i></a></li>';}
	if(get_field('options_linkedin_url','options')) {$output .= '<li><a class="social-button" href="'.get_field('options_linkedin_url','options').'" title="'.__('Follow Us On LinkedIn','_ntbp').'" target="_blank"><i class="fa fa-fw fa-linkedin"></i></a></li>';}
	if(get_field('options_pinterest_url','options')) {$output .= '<li><a class="social-button" href="'.get_field('options_pinterest_url','options').'" title="'.__('Follow Us On Pinterest','_ntbp').'" target="_blank"><i class="fa fa-fw fa-pinterest"></i></a></li>';}
	if(get_field('options_instagram_url','options')) {$output .= '<li><a class="social-button" href="'.get_field('options_instagram_url','options').'" title="'.__('Follow Us On Instagram','_ntbp').'" target="_blank"><i class="fa fa-fw fa-instagram"></i></a></li>';}
	$output .= '</ul>';
}
return $output;
}

/**
*	Options Address Output Function
*/
function _ntbp_get_the_address($format = 'inline', $icons = true) {
	$html = '';
	$options_email_address = get_field('options_email_address','options');
	$options_phone_number = get_field('options_phone_number','options');
	$options_fax_number = get_field('options_fax_number','options');
	$options_toll_free_number = get_field('options_toll_free_number','options');
	$options_street_address = get_field('options_street_address','options');
	$options_city = get_field('options_city','options');
	$options_province = get_field('options_province','options');
	$options_country = get_field('options_country','options');
	$options_postal_code = get_field('options_postal_code','options');
	if ($icons) {
		switch ($format) {
			case 'mailing':
			if($options_street_address){$html .= $options_street_address;}
			if($options_city){$html .= ', ' . $options_city;}
			if($options_province){$html .= ', ' . $options_province;}
			if($options_country){$html .= ', ' . $options_country;}
			if($options_postal_code){$html .= ', ' . $options_postal_code;}
			return $html;
			break;

			case 'mailing-stacked':
			if($options_street_address){$html .= $options_street_address;}
			if($options_city){$html .= '<br>' . $options_city;}
			if($options_province){$html .= ', ' . $options_province;}
			if($options_country){$html .= '<br>' . $options_country;}
			if($options_postal_code){$html .= '<br>' . $options_postal_code;}
			return $html;
			break;

			case 'street':
			if($options_street_address){$html .= $options_street_address;}
			if($options_city){$html .= ', ' . $options_city;}
			return $html;
			break;

			case 'phone':
			if($options_phone_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_phone_number.'"><i class="fa fa-fw fa-phone"></i>&nbsp;'.$options_phone_number.'</a>';}
			return $html;
			break;

			case 'tollfree':
			if($options_toll_free_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_toll_free_number.'"><i class="fa fa-fw fa-phone"></i>&nbsp;'.$options_toll_free_number.'</a>';}
			return $html;
			break;

			case 'fax':
			if($options_fax_number){$html .= '<a title="'.__('Send Us A Fax').'" href="tel:'.$options_fax_number.'"><i class="fa fa-fw fa-fax"></i>&nbsp;'.$options_fax_number.'</a>';}
			return $html;
			break;

			case 'email':
			if($options_email_address){$html .= '<a title="'.__('Send Us An Email').'" href="mailto:'.$options_email_address.'"><i class="fa fa-fw fa-envelope"></i>&nbsp;'.$options_email_address.'</a>';}
			return $html;
			break;

			default:
			if($options_phone_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_phone_number.'"><i class="fa fa-fw fa-phone"></i>&nbsp;'.$options_phone_number.'</a><br>';}
			if($options_toll_free_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_toll_free_number.'"><i class="fa fa-fw fa-phone"></i>&nbsp;'.$options_toll_free_number.'</a><br>';}
			if($options_fax_number){$html .= '<a title="'.__('Send Us A Fax').'" href="tel:'.$options_fax_number.'"><i class="fa fa-fw fa-fax"></i>&nbsp;'.$options_fax_number.'</a><br>';}
			if($options_email_address){$html .= '<a title="'.__('Send Us An Email').'" href="mailto:'.$options_email_address.'"><i class="fa fa-fw fa-envelope"></i>&nbsp;'.$options_email_address.'</a><br>';}
			if($options_street_address){$html .= '<br>' . $options_street_address;}
			if($options_city){$html .= '<br>' . $options_city;}
			if($options_province){$html .= ', ' . $options_province;}
			if($options_country){$html .= '<br>' . $options_country;}
			if($options_postal_code){$html .= '<br>' . $options_postal_code;}

			return $html;
		}
	} else {
		switch ($format) {
			case 'mailing':
			if($options_street_address){$html .= $options_street_address;}
			if($options_city){$html .= ', ' . $options_city;}
			if($options_province){$html .= ', ' . $options_province;}
			if($options_country){$html .= ', ' . $options_country;}
			if($options_postal_code){$html .= ', ' . $options_postal_code;}
			return $html;
			break;

			case 'mailing-stacked':
			if($options_street_address){$html .= $options_street_address;}
			if($options_city){$html .= '<br>' . $options_city;}
			if($options_province){$html .= ', ' . $options_province;}
			if($options_country){$html .= '<br>' . $options_country;}
			if($options_postal_code){$html .= '<br>' . $options_postal_code;}
			return $html;
			break;

			case 'street':
			if($options_street_address){$html .= $options_street_address;}
			if($options_city){$html .= ', ' . $options_city;}
			return $html;
			break;

			case 'phone':
			if($options_phone_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_phone_number.'">'.$options_phone_number.'</a>';}
			return $html;
			break;

			case 'tollfree':
			if($options_toll_free_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_toll_free_number.'">'.$options_toll_free_number.'</a>';}
			return $html;
			break;

			case 'fax':
			if($options_fax_number){$html .= '<a title="'.__('Send Us A Fax').'" href="tel:'.$options_fax_number.'">'.$options_fax_number.'</a>';}
			return $html;
			break;

			case 'email':
			if($options_email_address){$html .= '<a title="'.__('Send Us An Email').'" href="mailto:'.$options_email_address.'">'.$options_email_address.'</a>';}
			return $html;
			break;

			default:
			if($options_phone_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_phone_number.'">'.$options_phone_number.'</a><br>';}
			if($options_toll_free_number){$html .= '<a title="'.__('Give Us A Call').'" href="tel:'.$options_toll_free_number.'">'.$options_toll_free_number.'</a><br>';}
			if($options_fax_number){$html .= '<a title="'.__('Send Us A Fax').'" href="tel:'.$options_fax_number.'">'.$options_fax_number.'</a><br>';}
			if($options_email_address){$html .= '<a title="'.__('Send Us An Email').'" href="mailto:'.$options_email_address.'">'.$options_email_address.'</a><br>';}
			if($options_street_address){$html .= $options_street_address;}
			if($options_city){$html .= '<br>' . $options_city;}
			if($options_province){$html .= ', ' . $options_province;}
			if($options_country){$html .= '<br>' . $options_country;}
			if($options_postal_code){$html .= '<br>' . $options_postal_code;}

			return $html;
		}
	}
}

/**
*	Generate Flex Content Output
* Must be used within the Loop!
* @param none
* @return string
*/
function dig360_get_flex_content() {
	$output = '';
	if(have_rows('ext_pg_cnt_lyts')) :
		while(have_rows('ext_pg_cnt_lyts')) : the_row();
			switch (get_row_layout()) {
				case '1c':
				$output .= get_flex_1c();
				break;
				case '2c_5050':
				$output .= get_flex_2c_5050();
				break;
				case '2c_7030':
				$output .= get_flex_2c_7030();
				break;
				case '2c_3070':
				$output .= get_flex_2c_3070();
				break;
				case '3c':
				$output .= get_flex_3c();
				break;
				case '4c':
				$output .= get_flex_4c();
				break;
				case 'glly_sec':
				$output .= get_flex_gallery();
				break;
				case 'accd':
				$output .= get_flex_accordion();
				break;
				case 'tabs':
				$output .= get_flex_tabs();
				break;
				default:
				$output .= 'Error: No Matching Layout';
				break;
			}
		endwhile;
	endif;
	return $output;
}

/**
*	Compile Flex Content 1c Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_1c() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_1c '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';

	$output .= '<div class="small-12 columns">';
	$output .= ''.get_sub_field('c1');
	$output .= '</div><!-- .columns -->';

	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_1c -->';
	return $output;
}

/**
*	Compile Flex Content 2c_5050 Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_2c_5050() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_2c_5050 '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';

	$output .= '<div class="small-12 medium-6 columns">';
	$output .= ''.get_sub_field('c1');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-6 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';

	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_2c_5050 -->';
	return $output;
}

/**
*	Compile Flex Content 2c_7030 Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_2c_7030() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_2c_7030 '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';

	$output .= '<div class="small-12 medium-8 columns">';
	$output .= ''.get_sub_field('c1');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-4 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';

	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_2c_7030 -->';
	return $output;
}

/**
*	Compile Flex Content 2c_3070 Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_2c_3070() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_2c_3070 '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';

	$output .= '<div class="small-12 medium-4 columns">';
	$output .= ''.get_sub_field('c1');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-8 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';

	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_2c_3070 -->';
	return $output;
}

/**
*	Compile Flex Content 3c Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_3c() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_3c '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';

	$output .= '<div class="small-12 medium-4 columns">';
	$output .= ''.get_sub_field('c1');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-4 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-4 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';

	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_3c -->';
	return $output;
}

/**
*	Compile Flex Content 4c Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_4c() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_4c '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';
	$output .= '<div class="small-12 columns">';

	$output .= '<div class="small-12 medium-3 columns">';
	$output .= ''.get_sub_field('c1');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-3 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-3 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';
	$output .= '<div class="small-12 medium-3 columns">';
	$output .= ''.get_sub_field('c2');
	$output .= '</div><!-- .columns -->';

	$output .= '</div><!-- .columns -->';
	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_4c -->';
	return $output;
}

/**
*	Compile Flex Content gallery Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_gallery() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_accordion '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';
	$output .= '<div class="small-12 columns">';

	$output .= '<div data-featherlight-gallery data-featherlight-filter="a" class="row small-up-2 medium-up-4">';
	$gallery_images = get_sub_field('glly');
	if($gallery_images) :
		foreach($gallery_images as $gallery_image) :
			$output .= '<div class="column">';
			$output .= '<a class="gallery-thumbnail-link" href="'.$gallery_image['url'].'<img class="gallery-thumbnail thumbnail" src="'.$gallery_image['sizes']['thumbnail'].'" alt="'.$gallery_image['alt'].'"></a>';
			$output .= '</div>';
		endforeach;
	endif;
	$output .= '</div>';

	$output .= '</div><!-- .columns -->';
	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_accordion -->';
	return $output;
}

/**
*	Compile Flex Content accordion Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_accordion() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_accordion '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';
	$output .= '<div class="small-12 columns">';

	$output .= '<div class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">';
	if(have_rows('accordion_items')) :
		while(have_rows('accordion_items')) : the_row();
			$output .= '<div class="accordion-item" data-accordion-item>';
			$output .= '<a href="#" class="accordion-title">'.get_sub_field('accd_title').'</a>';
			$output .= '<div class="accordion-content" data-tab-content>';
			$output .= ''.get_sub_field('accd_content');
			$output .= '</div>';
			$output .= '</div>';
		endwhile;
	endif;
	$output .= '</div>';

	$output .= '</div><!-- .columns -->';
	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_accordion -->';
	return $output;
}

/**
*	Compile Flex Content tabs Output
* Called from dig360_get_flex_content()
* @param none
* @return string
*/
function get_flex_tabs() {
	$output = '';
	$sec_pad = (get_sub_field('sec_pad')) ? 'padded' : '';
	get_sub_field('sec_bg_cl') ? $sec_bg_cl = get_sub_field('sec_bg_cl') : $sec_bg_cl = '' ;
	$output .= '<section class="layout_section lyt_tabs '.$sec_pad.' '.$sec_bg_cl.'">';
	$output .= '<div class="row">';
	$output .= '<div class="small-12 columns">';

	if(have_rows('tab_items')) :
		$tab_group_id = uniqid('tab_group_');
		$tab_ids = array();
		$output .= '<ul class="tabs" data-tabs id="'.$tab_group_id.'">';
		$count = 0;
		while(have_rows('tab_items')) : the_row();
			$tab_id = uniqid('tab_');
			array_push($tab_ids, $tab_id);
			$active_class = ($count == 0) ? ' is-active ' : '';
			$active_aria = ($count == 0) ? ' aria-selected="true" ' : '';

			$output .= '<li class="tabs-title '.$active_class.'"><a href="#'.$tab_id.'" '.$active_aria.'>'.get_sub_field('tab_title').'</a></li>';

			$count++;
		endwhile;
		$output .= '</ul>';

		$output .= '<div class="tabs-content" data-tabs-content="'.$tab_group_id.'">';
		$count = 0;
		while(have_rows('tab_items')) : the_row();
			$tab_id = $tab_ids[$count];
			$active_class = ($count == 0) ? ' is-active ' : '';
			$c1 = get_sub_field('c1');
			$c2 = get_sub_field('c2');
			$dual_col_class = ($c2) ? ' medium-6 ' : '';

			$output .= '<div class="tabs-panel '.$active_class.'" id="'.$tab_id.'">';
			$output .= '<div class="row">';
			$output .= '<div class="small-12 '.$dual_col_class.' columns">'.$c1.'</div><!-- .columns -->';
			if($c2) :
				$output .= '<div class="small-12 '.$dual_col_class.' columns">'.$c2.'</div><!-- .columns -->';
			endif;
			$output .= '</div>';
			$output .= '</div>';

			$count++;
		endwhile;
		$output .= '</div>';

	endif;

	$output .= '</div><!-- .columns -->';
	$output .= '</div><!-- .row -->';
	$output .= '</section><!-- .layout_section.lyt_tabs -->';
	return $output;
}
