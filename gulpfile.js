// stylesheet directory path relative to this gulpfile
var stylesheet_dir = ".";

// modules
var gulp          = require('gulp');
var gutil         = require('gulp-util');
var sass          = require('gulp-sass');
var autoprefixer  = require('gulp-autoprefixer');
var coffee        = require('gulp-coffee');
var minifyCSS     = require('gulp-minify-css');
var uglify        = require('gulp-uglify');
var concat        = require('gulp-concat');
var merge2        = require('merge2');

// *** TIP *** To avoid bloating your GIT repo, install the required node modules
// globally then use the link command to create symbolic links to the modules
// $ npm install -g gulp gulp-util gulp-sass gulp-autoprefixer gulp-coffee gulp-minify-css gulp-uglify gulp-concat merge2
// $ npm link gulp gulp-util gulp-sass gulp-autoprefixer gulp-coffee gulp-minify-css gulp-uglify gulp-concat merge2

// compiling the SCSS files
gulp.task('scss', function () {
  gulp.src(stylesheet_dir + '/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['> 5%', 'last 2 versions'] }))
    .pipe(minifyCSS({keepSpecialComments: '*', keepBreaks: false}))
    .pipe(gulp.dest(stylesheet_dir + '/'));
});

gulp.task('scss-rtl', function () {
  gulp.src(stylesheet_dir + '/scss/rtl.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['> 5%', 'last 2 versions'] }))
    .pipe(minifyCSS({keepSpecialComments: '*', keepBreaks: false}))
    .pipe(gulp.dest(stylesheet_dir + '/'));
});

// compiling JS libraries
gulp.task('libraries', function () {
  gulp.src([
      stylesheet_dir + '/js/libraries/foundation.min.js',
      stylesheet_dir + '/js/libraries/*.js'
    ])
    .pipe(concat('libraries.js'))
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest(stylesheet_dir + '/js/'));
});

// compiling JS libraries and custom coffeescript into one minified file
gulp.task('javascript', function () {
  var includesStream = gulp.src(stylesheet_dir + '/js/libraries.js');

  var coffeeStream = gulp.src(stylesheet_dir + '/js/coffee/master.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(uglify({mangle: false}));

  merge2(includesStream, coffeeStream)
    .pipe(concat('site.js'))
    .pipe(gulp.dest(stylesheet_dir + '/js/'));
});

// watch for changes to scss, libraries and coffeescript files
gulp.task('default', function () {
  gulp.watch(stylesheet_dir + '/scss/**/*.scss', ['scss', 'scss-rtl']);

  gulp.watch(stylesheet_dir + '/js/libraries/**/*.js', ['libraries']);

  gulp.watch(stylesheet_dir + '/js/coffee/**/*.coffee', ['javascript']);
});
