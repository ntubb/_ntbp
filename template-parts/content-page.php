<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _ntbp
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="row entry-header">
		<?php if(has_post_thumbnail()) : ?>
			<div class="small-12 columns">
				<?php the_post_thumbnail(); ?>
			</div><!-- .columns -->
		<?php endif; ?>
		<?php if(!is_front_page()) : ?>
			<div class="small-12 columns">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</div><!-- .columns -->
		<?php endif; ?>

	</header><!-- .row.entry-header -->

	<div class="entry-content">
		<div class="row">
			<div class="small-12 columns">
				<?php the_content(); ?>
			</div><!-- .columns -->
		</div><!-- .row -->
		<?php get_template_part( 'template-parts/content', 'extended-page-layouts' ); ?>
		<div class="row">
			<div class="small-12 columns">
				<?php	wp_link_pages( array('before' => '<div class="page-links">' . esc_html__( 'Pages:', '_ntbp' ),'after'  => '</div>',	) );	?>
			</div><!-- .columns -->
		</div><!-- .row -->

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<div class="row">
			<div class="small-12 columns">
				<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', '_ntbp' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
						),
					'<span class="edit-link">',
					'</span>'
					);
					?>
				</div><!-- .columns -->
				</div><!-- .row -->
		</footer><!-- .entry-footer -->
	</article><!-- #post-## -->

