# Included Functions Files #
---------------------------------------------------

Here is where we keep the JavaScript files. We use GULP to compile the files 
from /libraries and /coffee into one minified JS file included in the site footer.

======================================================================================