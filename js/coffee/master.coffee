###*
# master.coffee
#
# Handles toggling the navigation menu for small screens and enables tab
# support for dropdown menus.
###

(($) ->
  # First Things First - Initialize Foundation JS
  $(document).foundation()

  # # Main Navigation Actions
  # # Menu Toggle Functions
  # toggle_mobile_nav = (the_toggle_button, the_nav, icon_class_open, icon_class_close) ->
  #   the_nav = $(the_nav)
  #   if the_nav.attr(data_dropdown_toggle_attr) == 'true'
  #     reset_all_toggles()
  #   else
  #     reset_all_toggles()
  #     the_nav.slideDown().attr data_dropdown_toggle_attr, 'true'
  #     the_toggle_button.find('> i').removeClass(icon_class_open).addClass icon_class_close
  #     return

  # # Reset All Toggles Function
  # reset_all_toggles = ->
  #   $('ul.menu, ul.sub-menu').filter(->
  #     if $(this).attr(data_dropdown_toggle_attr) != undefined
  #       return $(this)
  #     return
  #   ).fadeOut('fast').attr data_dropdown_toggle_attr, 'false'
  #   $('#primary-menu-toggle > i').removeClass('fa-close').addClass 'fa-bars'
  #   $('#contact-menu-toggle > i').removeClass('fa-angle-up').addClass 'fa-angle-down'
  #   return

  # # Handling the main navigation sub-menu drop-down toggling.
  # $('nav.site-navigation li.menu-item-has-children > a').click (e) ->
  #   this_sub_menu = $(e.target).closest('li.menu-item-has-children').find('ul.sub-menu').first()
  #   all_sub_menus = $('nav.site-navigation li.menu-item-has-children > ul.sub-menu')
  #   if this_sub_menu.attr(data_dropdown_toggle_attr) == 'true'
  #     this_sub_menu.attr data_dropdown_toggle_attr, 'false'
  #     this_sub_menu.fadeOut 200
  #   else
  #     all_sub_menus.attr data_dropdown_toggle_attr, 'false'
  #     all_sub_menus.fadeOut 200
  #     this_sub_menu.attr data_dropdown_toggle_attr, 'true'
  #     this_sub_menu.hide().fadeIn 200
  #   false

  # # Handling the full main menu toggle action on mobile.
  # $('#primary-menu-toggle').click (e) ->
  #   e.preventDefault()
  #   toggle_mobile_nav $(this), 'ul#primary-menu', 'fa-bars', 'fa-close'

  # # Handling the mobile quick links contact toggle action
  # $('#contact-menu-toggle').click (e) ->
  #   e.preventDefault()
  #   toggle_mobile_nav $(this), 'ul#contact-menu', 'fa-angle-down', 'fa-angle-up'

  # # On window resize make sure main nav is visibe on larger screens if hidden on a smaller one before.
  # $(window).resize (e) ->
  #   if $('header.site-header').width >= 1024
  #     console.log $('header.site-header').width
  #     $('ul#primary-menu').show()
  #   return

  # # Handling the outside 'clicks' to close all open toggles
  # $(document).click (e) ->
  #   if !$(event.target).closest('nav#header-navigation').length
  #     return reset_all_toggles()
  #   return

  # # Make sure that the main menu reappears if the window has been resized from small to large.
  # $(window).resize ->
  #   if $('body').width() >= 1024
  #     $('ul#primary-menu').show()
  #   false

  # # Back-to-top button action
  # scroll_duration = 1500
  # $('#back-to-top').click (e) ->
  #   e.preventDefault()
  #   $('html, body').animate { scrollTop: 0 }, scroll_duration
  #   false

  # # Scroll-down button action
  # $('#scroll-down-button').click (e) ->
  #   e.preventDefault
  #   $('html, body').animate { scrollTop: $('#scroll-here').offset().top }, scroll_duration
  #   false

  # # Handling jump links
  # $('a[href*=\\#]:not([href=\\#])').click ->
  #   if location.pathname.replace(/^\//, '') == @pathname.replace(/^\//, '') and location.hostname == @hostname
  #     target = $(@hash)
  #     target = if target.length then target else $('[name=' + @hash.slice(1) + ']')
  #     if target.length
  #       $('html,body').animate { scrollTop: target.offset().top }, scroll_duration
  #       return false
  #   return

  # Final Return DO NOT REMOVE
  return
) jQuery
