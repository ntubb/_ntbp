<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _ntbp
 */

?>

			</div><!-- #content -->

			<footer id="colophon" class="site-footer">
			<div class="site-info">
						<span class="site-copy left"><small><?php _e('&copy; '.date("Y").' <strong><a href="'.home_url( '/' ).'" rel="home">'.get_bloginfo('name').'</a></strong>');?></small></span>
						<span class="site-design right"><small><?php _e( 'Site Designed & Developed by <a href="http://wordpress.org/" target="_blank">Someone</a>' ); ?></small></span>
					</div><!-- .site-info -->
			</footer><!-- #colophon -->
		</div><!-- #page -->

		<?php wp_footer(); ?>

	</body>
</html>
