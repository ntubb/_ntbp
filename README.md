# Welcome to Nathaniel's Boilerplate WordPress Theme! #
---------------------------------------------------

This project is a starter theme pulling together the **Underscores (_s)** starter theme and
the **Zurb Foundation** framework. I have gone ahead and trimmed out bits and pieces from
Underscores, though not too much, leaving just what I need to get started on a new custom theme.

This theme uses the power of **SASS/SCSS** to configure and compile both the settings for the
Foundation framework as well as the theme's own custom styles. Using SASS/SCSS sometimes
takes a little bit to set up, but once the project compiling is configured it is really a
lot more productive for making changes throughout the development process.

### Some Tools I Use ###
* SASS - http://sass-lang.com/ - a CSS pre-compiler language for streamlined stylesheets
* CoffeeScript - http://coffeescript.org/ - a short-hand pre-compiler language for JavaScript
* Sublime Text 3 - http://www.sublimetext.com/ - my text editor of choice
* NodeJS - https://nodejs.org - the JavaScript runtime that powers Gulp
* Gulp - http://gulpjs.com/ - a CLI for automatic batch compiling of various source code
languages such as SASS and CoffeeScript

*See READMEs in specific directories for other important information on compiling and
other technical items.*

Below are the original instructions for initial configuration of the Underscores framework.
*It is advised to EXCLUDE files with these extensions (-*.scss, -*.js, -*.map, -*.md) when
running the find/replace actions!*

======================================================================================

Rename Theme and Functions
---------------

The first thing you want to do is copy the boilerplate theme directory and change the
name to something else (like, say, `awesometheme`), and then you'll need to do a
five-step find and replace on the name in all the templates.

1. Search for `'_ntbp'` (inside single quotations) to capture the text domain.
2. Search for `_ntbp_` to capture all the function names.
3. Search for `Text Domain: _ntbp` in style.css.
4. Search for <code>&nbsp;_ntbp</code> (with a space before it) to capture DocBlocks.
5. Search for `_ntbp-` to capture prefixed handles.

OR

* Search for: `'_ntbp'` and replace with: `'awesometheme'`
* Search for: `_ntbp_` and replace with: `awesometheme_`
* Search for: `Text Domain: _ntbp` and replace with: `Text Domain: awesometheme` in style.css.
* Search for: <code>&nbsp;_ntbp</code> and replace with: <code>&nbsp;awesometheme</code>
* Search for: `_ntbp-` and replace with: `awesometheme-`

Then, update the stylesheet header in `sass/style.scss` and the links in `footer.php`
with your own information. Next, update or delete this readme.

Now you're ready to go! The next step is easy to say, but harder to do: make an
awesome WordPress theme. :)

Good luck!

======================================================================================

GIT
---------------

Remove the .git directory from this theme so that it doesn't conflict with you own GIT project.

The included .gitignore file has some useful options for working with full WordPress sites in GIT

======================================================================================

GULP: SASS/SCSS, CoffeeScript and Compilers
---------------

I recommend using NodeJs with the GULP builder system to watch and compile your
SCSS and JS.

1. There is a sample gulpfile included which I suggest moving to the webroot
for your WordPress site.
2. Update the path to your theme and save the gulpfile. You may also change
preferences for your tasks and compilers in this file.
3. Install NodeJS if you don't already have it.
4. Use NPM to install the modules required to run GULP and the compilers.
You may either install them globally on your system and then link them to your
project, or install them locally within your project.

======================================================================================
