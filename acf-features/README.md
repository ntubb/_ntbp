# Advanced Custom Fields Features #
---------------------------------------------------

Here we have the export code for some useful extended flex-content layouts as well as some ACF Options Page fields.

You can create your own using the ACF plugin GUI and then export and load them from your theme.

======================================================================================