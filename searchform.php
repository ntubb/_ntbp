<form method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="row">
		<div class="large-12 columns">
			<div class="row collapse">
				<div class="small-10 columns">
					<label>
						<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
						<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
					</label>
				</div>
				<div class="small-2 columns">
					<input type="submit" class="search-submit button postfix" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</div>
			</div>
		</div>
	</div>
</form>