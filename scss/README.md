# Stylesheets - ./scss/ #
--------------------------------------------------------------------------------------

In this directory you will find the SASS/SCSS files for the theme split down into partials for easier organization. The original Underscores default SASS is grouped in its own partial with only the required, WordPress-specific, styles active. Most of the other styles there are handled by Foundation.

IMPORTANT!!!

When compiling `style.scss` this must compile to `../style.css` or in other words the theme's main stylesheet.

The right-to-left, `rtl.scss`, stylesheet should also be compiled to `../rtl.css`.

The Foundation components and `./settings/_settings.scss` are now set up to be imported directly into the theme's primary stylesheet. This is so that the variables and mixins available in the Foundation framework are more readily available for extended use in the theme's custom `./custom/*.scss` SASS/SCSS files.

======================================================================================