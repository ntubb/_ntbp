# Custom Stylesheets - ./scss/custom #
--------------------------------------------------------------------------------------

In this directory we store the SASS/SCSS partials for custom styles for this theme.
These are applied after the Foundation framework and inherit 
the variables and styles.

======================================================================================