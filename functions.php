<?php
/**
 * _ntbp functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _ntbp
 */

if ( ! function_exists( '_ntbp_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _ntbp_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _s, use a find and replace
	 * to change '_ntbp' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( '_ntbp', get_template_directory() . '/languages' );

	/*
	 * Add default posts and comments RSS feed links to head.
	 */
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Remove the admin bar from front-facing pages.
	 */
	add_filter('show_admin_bar', '__return_false');

	/*
	 * Set default image link to none when inserting images from the Media Library.
	 */
	update_option('image_default_link_type','none');

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Enable support for Menus.
	 *
	 */
	add_theme_support( 'menus' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	// add_theme_support( 'post-formats', array(
	// 	'aside',
	// 	'image',
	// 	'video',
	// 	'quote',
	// 	'link',
	// ) );

	/*
	* This theme uses wp_nav_menu() in multiple locations.
	*
	*/
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', '_ntbp' ),
		'secondary' => esc_html__( 'Secondary Menu', '_ntbp' ),
		'mobile' => esc_html__( 'Mobile Quick Links Menu', '_ntbp' ),
		) );

	/*
	* This theme uses custom image sizes for specific layout locations and responsive picture elements.
	*
	*/
	add_image_size( 'wp-logo', 		400, 400, false );
	add_image_size( 'wp-wide', 		1920, 750, true );
	add_image_size( 'wp-large', 	1440, 562, true );
	add_image_size( 'wp-medium', 	960, 	375, true );
	add_image_size( 'wp-small', 	640, 	250, true );

	/*
	* Allowing custom image sizes to be chosen from the Media Library.
	*
	*/
	// add_filter( 'image_size_names_choose', '_ntbp_custom_sizes' );
	// function _ntbp_custom_sizes( $sizes ) {
	// 	return array_merge( $sizes, array(
	// 		'wp-logo' => __( 'Your Custom Size Name' ),
	// 		'wp-wide' => __( 'Your Custom Size Name' ),
	// 		'wp-large' => __( 'Your Custom Size Name' ),
	// 		'wp-medium' => __( 'Your Custom Size Name' ),
	// 		'wp-small' => __( 'Your Custom Size Name' ),
	// 		) );
	// }

}
endif; // _ntbp_setup
add_action( 'after_setup_theme', '_ntbp_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
// function _ntbp_content_width() {
// 	$GLOBALS['content_width'] = apply_filters( '_ntbp_content_width', 640 );
// }
// add_action( 'after_setup_theme', '_ntbp_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function _ntbp_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar One', '_ntbp' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
}
add_action( 'widgets_init', '_ntbp_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function _ntbp_scripts() {

	/* Add Site Styles
	 * Grab the date modified time for the JS file and set as the version number for the script.
	 * Helps with updating the caching of scripts on servers and in-browser to reflect lasted code.
	 */
	$site_style_mtime = filemtime(get_template_directory() . '/style.css');
	wp_enqueue_style('site-style', get_stylesheet_uri(), false, $site_style_mtime );

	if(!is_admin()) {

		/* Add Site JS
		 * Grab the date modified time for the JS file and set as the version number for the script.
		 * Helps with updating the caching of scripts on servers and in-browser to reflect lasted code.
		 */
  	$site_js_mtime = filemtime(get_template_directory() . '/js/site.js');
		wp_enqueue_script( '_ntbp-site', get_template_directory_uri() . '/js/site.js', array(), '20160506', true );

	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', '_ntbp_scripts' );

/**
 * Admin functions to hide certain unwanted dashboard options.
 */
function _ntbp_remove_admin_bar_nodes( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'wp-logo' );
  $wp_admin_bar->remove_node( 'comments' );
  $wp_admin_bar->remove_node( 'new-content' );
}
add_action( 'admin_bar_menu', '_ntbp_remove_admin_bar_nodes', 999 );

function _ntbp_remove_menus(){
  // remove_menu_page( 'index.php' );                  //Dashboard
  // remove_menu_page( 'edit.php' );                   //Posts
  // remove_menu_page( 'upload.php' );                 //Media
  // remove_menu_page( 'edit.php?post_type=page' );    //Pages
  // remove_menu_page( 'edit-comments.php' );          //Comments
  // remove_menu_page( 'themes.php' );                 //Appearance
  // remove_menu_page( 'plugins.php' );                //Plugins
  // remove_menu_page( 'users.php' );                  //Users
  // remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings
  // remove_submenu_page( 'themes.php', 'themes.php' );
  remove_submenu_page( 'themes.php', 'theme-editor.php' );
  remove_submenu_page( 'themes.php', 'customize.php' );

  // global $submenu;
  // unset($submenu['tools.php'][5]); // Available Tools
  // unset($submenu['tools.php'][10]); // Import
  // unset($submenu['tools.php'][15]); // Export
  // unset($submenu['plugins.php'][15]); // Editor
  // unset($submenu['themes.php'][5]); // Themes
  // unset($submenu['themes.php'][6]); // Customize
  // unset($submenu['themes.php'][11]); // Editor
}
add_action( 'admin_menu', '_ntbp_remove_menus', 110 );

function _ntbp_disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', '_ntbp_disable_emojis' );

/**
 * Custom ACF Features for this theme.
 */
// require get_template_directory() . '/acf-features/acf-features.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/filters.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';
